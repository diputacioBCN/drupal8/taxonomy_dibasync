<?php

namespace Drupal\taxonomy_dibasync\Services;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\taxonomy_dibasync\Services\DataCollectorInterface;
use Drupal\taxonomy_dibasync\Services\DataCollectorTrait;
use GuzzleHttp\ClientInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Comarques i Municipis DiBa from Diba OpenData Rest API.
 */
class ComarquesMunicipisDiba implements DataCollectorInterface, ContainerInjectionInterface {

  use DataCollectorTrait;
  use MessengerTrait;

  /**
   * The module config.
   */
  protected $config;

  /**
   * The Entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The http client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Mapping dataset/vocabulary settings.
   */
  private $settings = [
    'municipis' => [
      'fields' => [
        'name' => [
          'name'   => 'Nom',
          'origin' => 'municipi_nom',
        ],
        'field_mun_codi_ine' => [
          'name'   => 'Codi INE',
          'origin' => 'ine',
        ],
        'field_com_codi_comarca' => [
          'name'   => 'Codi de comarca',
          'origin' => 'grup_comarca.comarca_codi',
        ],
      ],
      'fields_map' => [
        'field_mun_codi_ine' => 'ine',
        'name'               => 'municipi_nom',
        'name'               => 'municipi_nom_curt',
      ],
    ],
    'comarques' => [
      'fields' => [
        'name' => [
          'name'   => 'Nom',
          'origin' => 'comarca_nom',
        ],
        'field_com_codi_comarca' => [
          'name'   => 'Codi de comarca',
          'origin' => 'comarca_id',
        ],
      ],
      'fields_map' => [
        'field_com_codi_comarca' => 'comarca_id',
        'name'                   => 'comarca_nom',
      ],
    ],
    'comarques_municipis' => [
      'fields' => [
        'name' => [
          'name'   => 'Nom',
          'origin' => ['comarca_nom', 'municipi_nom'],
        ],
        'field_com_codi_comarca' => [
          'name'   => 'Codi de comarca',
          'origin' => ['comarca_id', 'grup_comarca.comarca_codi'],
        ],
        'field_mun_codi_ine' => [
          'name'   => 'Codi INE',
          'origin' => 'ine',
        ],
        'field_ens_tipus' => [
          'name'   => "Tipus d'ens",
          'origin' => 'tipus',
        ],
      ],
      'comarca_id_field' => 'field_com_codi_comarca',
      'filter_field'  => 'field_ens_tipus',
    ],
  ];

  /**
   * Sync data with drupal vocabulary.
   */
  public function syncData(string $type) {
    $vid = $this->config->get($type . '_target');

    // Pass settings trought massage function to implement hook alters.
    $settings = $this->massageSettings($type, $this->settings);

    if (!empty($vid) && $this->buildVocabularyFields($vid, $settings[$type]['fields'])) {
      $data = '';

      if ('comarques_municipis' === $type) {
        $config = [
          'fields'           => $settings[$type]['fields'],
          'missing_action'   => $this->config->get('comarques_municipis_missing'),
          'duplicate_action' => $this->config->get('comarques_municipis_duplicate'),
        ];

        // Càrrega de comarques.
        $comarques = $this->getData('comarques');
        $comarques = $this->setDataField($comarques, 'tipus', 'comarca');

        $config['fields_map'] = $settings['comarques']['fields_map'];
        $config['filter_terms'] = [$settings[$type]['filter_field'] => 'comarca'];
        $this->syncDataTerms($vid, $comarques, $config);

        // Càrrega de municipis.
        $municipis = $this->getData('municipis');
        $municipis = $this->setDataField($municipis, 'tipus', 'municipi');

        $config['fields_map'] = $settings['municipis']['fields_map'];
        $config['filter_terms'] = [$settings[$type]['filter_field'] => 'municipi'];
        $this->syncDataTerms($vid, $municipis, $config);

        // Comarca parent dels municipis.
        $terms = $this->loadTerms($vid, [$settings[$type]['filter_field'] => 'municipi']);
        if (!empty($terms)) {
          foreach ($terms as $term) {
            $tid_pare = '';
            if ($codi_pare = $term->get($settings[$type]['comarca_id_field'])->getString()) {
              $tid_pare = $this->getTidComarca($vid, $settings[$type]['comarca_id_field'], $settings[$type]['filter_field'], $codi_pare);
            }
            if (!empty($tid_pare)) {
              $term->set('parent', $tid_pare)->save();
            }
          }
        }
      }
      else {
        // Carrega normal d'un dataset en format llista.
        $data = $this->getData($type);

        if (!empty($data)) {
          $config = [
            'fields'           => $settings[$type]['fields'],
            'fields_map'       => $settings[$type]['fields_map'],
            'missing_action'   => $this->config->get($type . '_missing'),
            'duplicate_action' => $this->config->get($type . '_duplicate'),
          ];
          $this->syncDataTerms($vid, $data, $config);
        }
      }
    }
  }

  /**
   * Get collector data.
   */
  private function getData($dataset) {
    $data = [];

    $url = rtrim($this->config->get('opendata_url'), '/') . '/dataset/' . $dataset;
    $token = $this->config->get('opendata_token');
    if (!empty($token)) {
      $url .= '/token/' . $token;
    }

    $response = $this->httpClient->request('GET', $url, ['verify' => FALSE]);
    if ($response->getStatusCode() == 200) {
      $response_data = json_decode($response->getBody(), TRUE);

      if (!empty($response_data['elements'])) {
        $data = $response_data['elements'];
      }
      else {
        $this->logger->get('taxonomy_dibasync')->info(new FormattableMarkup('Unable to retry data from @url.', [
          '@url' => $url,
        ]));
      }
    }
    else {
      $this->logger->get('taxonomy_dibasync')->info(new FormattableMarkup('Unable to connect to @url. Status code response: @code', [
        '@url' => $url,
        '@code' => $response->getStatusCode(),
      ]));
    }

    return $data;
  }

  /**
   * Set field value in all dataset items.
   */
  private function setDataField($dataset, $field, $value) {
    if (!empty($dataset)) {
      foreach ($dataset as $key => $item) {
        $dataset[$key][$field] = $value;
      }
    }

    return $dataset;
  }

  /**
   * Torna el tid de la comarca a partir del codi de comarca.
   */
  private function getTidComarca($vid, $comarca_id_field, $filter_field, $codi_comarca) {
    $tid = $this->entityTypeManager
      ->getStorage('taxonomy_term')
      ->getQuery()
      ->condition('vid', $vid)
      ->condition($comarca_id_field, $codi_comarca)
      ->condition($filter_field, 'comarca')
      ->accessCheck(FALSE)
      ->execute();

    return !empty($tid) ? reset($tid) : '';
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityFieldManagerInterface $entity_field_manager, EntityTypeManagerInterface $entity_type_manager, ClientInterface $http_client, $logger, ModuleHandlerInterface $module_handler) {
    $this->config = $config_factory->get('taxonomy_dibasync.settings');
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->httpClient = $http_client;
    $this->logger = $logger;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_field.manager'),
      $container->get('entity_type.manager'),
      $container->get('http_client'),
      $container->get('logger.factory'),
      $container->get('module_handler')
    );
  }

}
