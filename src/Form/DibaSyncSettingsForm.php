<?php

namespace Drupal\taxonomy_dibasync\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\Entity\Vocabulary;

/**
 * Configure Vus settings for this site.
 */
class DibaSyncSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'dibasync_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['taxonomy_dibasync.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('taxonomy_dibasync.settings');
    $vocabularies = $this->getVocabularies();
    $resync_freqs = $this->getResyncFrequencies();
    $delete_modes = $this->getDeleteModes();

    $form['left'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => ['class' => ['layout-column', 'layout-column--half']],
    ];
    $form['right'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => ['class' => ['layout-column', 'layout-column--half']],
    ];
    $form['left']['ora'] = [
      '#type' => 'details',
      '#title' => $this->t('@label settings', ['@label' => 'Oracle']),
      '#open' => TRUE,
      '#description' => t('Diba Oracle conexion settings.'),
    ];
    $form['left']['ora']['ora_user'] = [
      '#type' => 'textfield',
      '#title' => $this->t('@label username', ['@label' => 'Oracle']),
      '#default_value' => $config->get('ora_user'),
    ];
    $form['left']['ora']['ora_pass'] = [
      '#type' => 'password',
      '#title' => $this->t('@label password', ['@label' => 'Oracle']),
      '#default_value' => $config->get('ora_pass'),
      '#description' => t('Leave blank if it is set and you do not want to change it.'),
    ];
    $form['left']['ora']['ora_host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('@label database host', ['@label' => 'Oracle']),
      '#default_value' => $config->get('ora_host'),
    ];
    $form['left']['ora']['ora_charset'] = [
      '#type' => 'textfield',
      '#title' => $this->t('@label charset', ['@label' => 'Oracle']),
      '#default_value' => $config->get('ora_charset'),
    ];
    $form['left']['ora']['ora_iso_charset'] = [
      '#type' => 'textfield',
      '#title' => $this->t('@label charset', ['@label' => 'Oracle ISO']),
      '#default_value' => $config->get('ora_iso_charset'),
    ];
    $form['left']['ora']['drupal_charset'] = [
      '#type' => 'textfield',
      '#title' => $this->t('@label charset', ['@label' => 'Drupal']),
      '#default_value' => $config->get('drupal_charset'),
    ];

    $form['right']['opendata'] = [
      '#type' => 'details',
      '#title' => $this->t('@label settings', ['@label' => 'OpenData']),
      '#open' => TRUE,
      '#description' => t('Diba OpenData Rest API settings.'),
    ];
    $form['right']['opendata']['opendata_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('@label token', ['@label' => 'OpenData Rest API']),
      '#default_value' => $config->get('opendata_token'),
      '#description' => t('Optional but recommended for statistical purposes.'),
    ];
    $form['right']['opendata']['opendata_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('@label base path url', ['@label' => 'OpenData Rest API']),
      '#default_value' => $config->get('opendata_url'),
    ];

    $form['left']['ora']['organigrama'] = [
      '#type' => 'details',
      '#title' => $this->t('@label settings', ['@label' => 'Organigrama']),
      '#description' => $this->t('Organigrama tree using SAP Codes to determine parents.'),
      '#open' => TRUE,
    ];
    $form['left']['ora']['organigrama']['organigrama_level'] = [
      '#type' => 'number',
      '#title' => $this->t('@label depth level', ['@label' => 'Organigrama']),
      '#default_value' => $config->get('organigrama_level'),
      '#min' => 0,
      '#max' => 10,
      '#step' => 1,
    ];
    $form['left']['ora']['organigrama']['organigrama_target'] = [
      '#type' => 'select',
      '#title' => $this->t('Target vocabulary'),
      '#default_value' => $config->get('organigrama_target'),
      '#options' => $vocabularies,
      '#empty_option' => $this->t('- None -'),
      '#empty_value' => '_none',
      '#description' => t('Leave target blank if you do not want to synchronize this dataset.'),
    ];
    $form['left']['ora']['organigrama']['organigrama_cron'] = [
      '#type' => 'select',
      '#title' => $this->t('Resync frequency'),
      '#default_value' => $config->get('organigrama_cron'),
      '#options' => $resync_freqs,
      '#empty_option' => $this->t('- None -'),
      '#empty_value' => '_none',
    ];
    $form['left']['ora']['organigrama']['organigrama_missing'] = [
      '#type' => 'select',
      '#title' => $this->t('Action on missing input item'),
      '#default_value' => $config->get('organigrama_missing'),
      '#options' => $delete_modes,
      '#empty_option' => $this->t('- None -'),
      '#empty_value' => '_none',
    ];
    $form['left']['ora']['organigrama']['organigrama_duplicate'] = [
      '#type' => 'select',
      '#title' => $this->t('Action on term duplicate'),
      '#default_value' => $config->get('organigrama_duplicate'),
      '#options' => $delete_modes,
      '#empty_option' => $this->t('- None -'),
      '#empty_value' => '_none',
    ];

    $form['right']['opendata']['municipis'] = [
      '#type' => 'details',
      '#title' => $this->t('@label settings', ['@label' => 'Municipis']),
      '#description' => $this->t('Municipis plain list with INE codes.'),
      '#open' => TRUE,
    ];
    $form['right']['opendata']['municipis']['municipis_target'] = [
      '#type' => 'select',
      '#title' => $this->t('Target vocabulary'),
      '#default_value' => $config->get('municipis_target'),
      '#options' => $vocabularies,
      '#empty_option' => $this->t('- None -'),
      '#empty_value' => '_none',
      '#description' => t('Leave target blank if you do not want to synchronize this dataset.'),
    ];
    $form['right']['opendata']['municipis']['municipis_cron'] = [
      '#type' => 'select',
      '#title' => $this->t('Resync frequency'),
      '#default_value' => $config->get('municipis_cron'),
      '#options' => $resync_freqs,
      '#empty_option' => $this->t('- None -'),
      '#empty_value' => '_none',
    ];
    $form['right']['opendata']['municipis']['municipis_missing'] = [
      '#type' => 'select',
      '#title' => $this->t('Action on missing input item'),
      '#default_value' => $config->get('municipis_missing'),
      '#options' => $delete_modes,
      '#empty_option' => $this->t('- None -'),
      '#empty_value' => '_none',
    ];
    $form['right']['opendata']['municipis']['municipis_duplicate'] = [
      '#type' => 'select',
      '#title' => $this->t('Action on term duplicate'),
      '#default_value' => $config->get('municipis_duplicate'),
      '#options' => $delete_modes,
      '#empty_option' => $this->t('- None -'),
      '#empty_value' => '_none',
    ];

    $form['right']['opendata']['comarques'] = [
      '#type' => 'details',
      '#title' => $this->t('@label settings', ['@label' => 'Comarques']),
      '#description' => $this->t('Comarques plain list.'),
      '#open' => TRUE,
    ];
    $form['right']['opendata']['comarques']['comarques_target'] = [
      '#type' => 'select',
      '#title' => $this->t('Target vocabulary'),
      '#default_value' => $config->get('comarques_target'),
      '#options' => $vocabularies,
      '#empty_option' => $this->t('- None -'),
      '#empty_value' => '_none',
      '#description' => t('Leave target blank if you do not want to synchronize this dataset.'),
    ];
    $form['right']['opendata']['comarques']['comarques_cron'] = [
      '#type' => 'select',
      '#title' => $this->t('Resync frequency'),
      '#default_value' => $config->get('comarques_cron'),
      '#options' => $resync_freqs,
      '#empty_option' => $this->t('- None -'),
      '#empty_value' => '_none',
    ];
    $form['right']['opendata']['comarques']['comarques_missing'] = [
      '#type' => 'select',
      '#title' => $this->t('Action on missing input item'),
      '#default_value' => $config->get('comarques_missing'),
      '#options' => $delete_modes,
      '#empty_option' => $this->t('- None -'),
      '#empty_value' => '_none',
    ];
    $form['right']['opendata']['comarques']['comarques_duplicate'] = [
      '#type' => 'select',
      '#title' => $this->t('Action on term duplicate'),
      '#default_value' => $config->get('comarques_duplicate'),
      '#options' => $delete_modes,
      '#empty_option' => $this->t('- None -'),
      '#empty_value' => '_none',
    ];

    $form['right']['opendata']['comarques_municipis'] = [
      '#type' => 'details',
      '#title' => $this->t('@label settings', ['@label' => 'Comarques/Municipis']),
      '#description' => $this->t('Comarques>Municipis tree.'),
      '#open' => TRUE,
    ];
    $form['right']['opendata']['comarques_municipis']['comarques_municipis_target'] = [
      '#type' => 'select',
      '#title' => $this->t('Target vocabulary'),
      '#default_value' => $config->get('comarques_municipis_target'),
      '#options' => $vocabularies,
      '#empty_option' => $this->t('- None -'),
      '#empty_value' => '_none',
      '#description' => t('Leave target blank if you do not want to synchronize this dataset.'),
    ];
    $form['right']['opendata']['comarques_municipis']['comarques_municipis_cron'] = [
      '#type' => 'select',
      '#title' => $this->t('Resync frequency'),
      '#default_value' => $config->get('comarques_municipis_cron'),
      '#options' => $resync_freqs,
      '#empty_option' => $this->t('- None -'),
      '#empty_value' => '_none',
    ];
    $form['right']['opendata']['comarques_municipis']['comarques_municipis_missing'] = [
      '#type' => 'select',
      '#title' => $this->t('Action on missing input item'),
      '#default_value' => $config->get('comarques_municipis_missing'),
      '#options' => $delete_modes,
      '#empty_option' => $this->t('- None -'),
      '#empty_value' => '_none',
    ];
    $form['right']['opendata']['comarques_municipis']['comarques_municipis_duplicate'] = [
      '#type' => 'select',
      '#title' => $this->t('Action on term duplicate'),
      '#default_value' => $config->get('comarques_municipis_duplicate'),
      '#options' => $delete_modes,
      '#empty_option' => $this->t('- None -'),
      '#empty_value' => '_none',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('taxonomy_dibasync.settings')
      ->set('ora_user', $form_state->getValue('ora_user'))
      ->set('ora_host', $form_state->getValue('ora_host'))
      ->set('ora_charset', $form_state->getValue('ora_charset'))
      ->set('ora_iso_charset', $form_state->getValue('ora_iso_charset'))
      ->set('drupal_charset', $form_state->getValue('drupal_charset'))
      ->set('opendata_token', $form_state->getValue('opendata_token'))
      ->set('opendata_url', $form_state->getValue('opendata_url'))
      ->set('organigrama_level', $form_state->getValue('organigrama_level'))
      ->set('organigrama_target', $form_state->getValue('organigrama_target'))
      ->set('organigrama_cron', $form_state->getValue('organigrama_cron'))
      ->set('organigrama_missing', $form_state->getValue('organigrama_missing'))
      ->set('organigrama_duplicate', $form_state->getValue('organigrama_duplicate'))
      ->set('municipis_target', $form_state->getValue('municipis_target'))
      ->set('municipis_cron', $form_state->getValue('municipis_cron'))
      ->set('municipis_missing', $form_state->getValue('municipis_missing'))
      ->set('municipis_duplicate', $form_state->getValue('municipis_duplicate'))
      ->set('comarques_target', $form_state->getValue('comarques_target'))
      ->set('comarques_cron', $form_state->getValue('comarques_cron'))
      ->set('comarques_missing', $form_state->getValue('comarques_missing'))
      ->set('comarques_duplicate', $form_state->getValue('comarques_duplicate'))
      ->set('comarques_municipis_target', $form_state->getValue('comarques_municipis_target'))
      ->set('comarques_municipis_cron', $form_state->getValue('comarques_municipis_cron'))
      ->set('comarques_municipis_missing', $form_state->getValue('comarques_municipis_missing'))
      ->set('comarques_municipis_duplicate', $form_state->getValue('comarques_municipis_duplicate'));

    // Not remove ora pass if is not set.
    if (!empty($form_state->getValue('ora_pass'))) {
      $this->config('taxonomy_dibasync.settings')->set('ora_pass', $form_state->getValue('ora_pass'));
    }

    $this->config('taxonomy_dibasync.settings')->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Get site vocabularies list.
   */
  private function getVocabularies() {
    $vocabularies = Vocabulary::loadMultiple();
    $vocabs = [];
    foreach ($vocabularies as $vid => $vocabulary) {
      $vocabs[$vid] = $vocabulary->label();
    }

    return $vocabs;
  }

  /**
   * Get avaible delete modes.
   */
  private function getDeleteModes() {
    return [
      'unpublish' => $this->t('Unpublish'),
      'delete'    => $this->t('Delete'),
      'ignore'    => $this->t('Ignore'),
    ];
  }

  /**
   * Get avaible resync frequencies.
   */
  private function getResyncFrequencies() {
    return [
      0        => $this->t('Every cron run'),
      3600     => $this->t('1 hour'),
      43200    => $this->t('@hours hours', ['@hours' => 12]),
      86400    => $this->t('1 day'),
      604800   => $this->t('1 week'),
      2592000  => $this->t('1 month'),
      7776000  => $this->t('@months months', ['@months' => 3]),
      31536000 => $this->t('1 year'),
      94608000 => $this->t('@years years', ['@years' => 3]),
    ];
  }

}
