<?php

/**
 * @file
 * Hooks specific examples to the Taxonomy dibasync module.
 */

/**
 * Implements HOOK_taxonomy_dibasync_settings_TYPE_alter() example.
 */
function hook_taxonomy_dibasync_settings_organigrama_alter(&$settings) {
  // Add a new field.
  $settings['fields'][] = [
    'field_my_extra_field' => [
      'name' => 'My extra field',
    ],
  ];
  // Remove unused field from vocabulary.
  unset($settings['fields']['field_org_reduit']);
}

/**
 * Implements hook_taxonomy_dibasync_sync_dataset_VID_alter() example.
 */
function hook_taxonomy_dibasync_sync_dataset_my_vocabulary_alter(&$dataset) {
  // Add a conditional custom value to the dataset.
  foreach ($dataset as $key => $item) {
    foreach ($item as $field => $value) {
      if ('ine' === $field) {
        if ('08' === substr($value, 0, 2)) {
          $dataset[$key]['dataset_custom_value'] = 'Municipi BCN';
        }
        else {
          $dataset[$key]['dataset_custom_value'] = 'Municipi de fora';
        }
      }
    }
  }
}

/**
 * Implements hook_taxonomy_dibasync_sync_config_VID_alter() example.
 */
function hook_taxonomy_dibasync_sync_config_my_vocabulary_alter(&$config) {
  // Assign to my extra field the dataset_custom_value set to dataset values.
  $config['fields_map'][] = [
    'field_my_extra_field' => 'dataset_custom_value',
  ];
  // Not sincronize this field.
  unset($config['fields']['codi_comarca']);
}
