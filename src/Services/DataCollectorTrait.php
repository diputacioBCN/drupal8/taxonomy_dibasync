<?php

namespace Drupal\taxonomy_dibasync\Services;

use Drupal\Component\Render\FormattableMarkup;

/**
 * Trait with helper methods for data collectors.
 */
trait DataCollectorTrait {

  /**
   * Massage settings to invoke alter hooks.
   */
  private function massageSettings($type, $settings) {
    $this->moduleHandler->alter('taxonomy_dibasync_settings_' . $type, $settings);

    return $settings;
  }

  /**
   * Torna la llista de termes.
   */
  private function loadTerms($vid, $filters = []) {
    $query = $this->entityTypeManager
      ->getStorage('taxonomy_term')
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('vid', $vid);

    if (!empty($filters)) {
      foreach ($filters as $key => $value) {
        $query->condition($key, $value);
      }
    }
    $tids = $query->execute();

    $terms = [];
    if (!empty($tids)) {
      $terms = $this->entityTypeManager
        ->getStorage('taxonomy_term')
        ->loadMultiple($tids);
    }

    return $terms;
  }

  /**
   * Creates vocabulary fields if not exists.
   */
  private function buildVocabularyFields($vid, $fields) {
    $vocabulary = $this->entityTypeManager->getStorage('taxonomy_vocabulary')->load($vid);

    if (!empty($vocabulary)) {
      $definitions = $this->entityFieldManager->getFieldDefinitions('taxonomy_term', $vid);

      foreach ($fields as $key => $value) {
        if (!isset($definitions[$key])) {
          $field_storage = $this->entityTypeManager
            ->getStorage('field_storage_config')
            ->load('taxonomy_term.' . $key);

          // Si no exiteix el camp creem-lo.
          if (empty($field_storage)) {
            $field_storage = $this->entityTypeManager
              ->getStorage('field_storage_config')
              ->create([
                'field_name'  => $key,
                'entity_type' => 'taxonomy_term',
                'type'        => 'string',
              ])->save();
          }

          // Assignem el camp al vocabulari.
          $field_config = $this->entityTypeManager
            ->getStorage('field_config')
            ->create([
              'field_name'  => $key,
              'entity_type' => 'taxonomy_term',
              'label'       => $value['name'],
              'bundle'      => $vid,
            ])->save();
        }
      }

      return TRUE;
    }

    return FALSE;
  }

  /**
   * Sync dataset data with vocabulary data terms.
   */
  private function syncDataTerms($vid, $dataset, $config) {
    // Invoke hooks hook_taxonomy_dibasync_sync_dataset_VID_alter().
    $this->moduleHandler->alter('taxonomy_dibasync_sync_dataset_' . $vid, $dataset);
    // Invoke hooks hook_taxonomy_dibasync_sync_config_VID_alter().
    $this->moduleHandler->alter('taxonomy_dibasync_sync_config_' . $vid, $config);

    if (empty($vid) || empty($dataset) || empty($config['fields'])) {
      $this->logger->get('taxonomy_dibasync')->error(new FormattableMarkup('Missing data from terms sinconization. Vid: "@vid", dataset count: @dataset, config count: @config', [
        '@vid'     => $vid,
        '@dataset' => count($dataset),
        '@config'  => count($config),
      ]));

      return FALSE;
    }

    // Contadors de les accions fetes.
    $new = $update = $delete = $unpublish = 0;

    // Recuperem els valors actuals i mirem quins coincideixen.
    $filters = isset($config['filter_terms']) ? $config['filter_terms'] : [];
    $terms = $this->loadTerms($vid, $filters);
    if (!empty($terms)) {
      // Collect search values.
      $search_values = [];
      foreach ($config['fields_map'] as $term_key => $dataset_key) {
        $search_values[$term_key] = array_column($dataset, $dataset_key);
      }

      foreach ($terms as $term) {
        $key = FALSE;
        foreach ($search_values as $term_key => $values_list) {
          if (FALSE === $key) {
            $key = array_search($term->get($term_key)->getString(), $values_list);
          }
        }

        // If we have a match, we can update the term data.
        if (FALSE !== $key) {
          if (isset($dataset[$key])) {
            $term = $this->updateTermFields($term, $dataset[$key], $config['fields']);
            $term->setPublished(TRUE)->save();
            $update++;
            // Esborrem el terme de la llista de l'organigrama. Després
            // farem servir la llista de supervivents per crear nous termes.
            unset($dataset[$key]);
          }
          else {
            // És un duplicat, ho notifiquem i actuem.
            if ('delete' === $config['duplicate_action']) {
              $term->delete();
              $delete++;

              $this->logger->get('taxonomy_dibasync')->info(new FormattableMarkup('Syncronizing terms in vid "@vid". Duplicated term @label (@tid) deleted.', [
                '@vid' => $vid,
                '@label' => $term->label(),
                '@tid' => $term->id(),
              ]));
            }
            else {
              if ('unpublish' === $config['duplicate_action']) {
                $term->setPublished(FALSE)->save();
                $unpublish++;

                $this->logger->get('taxonomy_dibasync')->info(new FormattableMarkup('Syncronizing terms in vid "@vid". Duplicated term @label (@tid) unpublished.', [
                  '@vid' => $vid,
                  '@label' => $term->label(),
                  '@tid'   => $term->id(),
                ]));
              }
            }
          }
        }
        else {
          // Ja no existeix = DELETE. Esborrat lògic, físic o ignora.
          if ('unpublish' === $config['missing_action']) {
            $term->setPublished(FALSE)->save();
            $unpublish++;

            $this->logger->get('taxonomy_dibasync')->info(new FormattableMarkup('Syncronizing terms in vid "@vid". Missing term @label (@tid) unpublished.', [
              '@vid'   => $vid,
              '@label' => $term->label(),
              '@tid'   => $term->id(),
            ]));
          }
          elseif ('delete' === $config['missing_action']) {
            $term->delete();
            $delete++;
            $this->logger->get('taxonomy_dibasync')->info(new FormattableMarkup('Syncronizing terms in vid "@vid". Missing term @label (@tid) deleted.', [
              '@vid'   => $vid,
              '@label' => $term->label(),
              '@tid'   => $term->id(),
            ]));
          }
        }
      }
    }

    // Nous termes = CREATE.
    foreach ($dataset as $terme) {
      $term = $this->entityTypeManager->getStorage('taxonomy_term')->create([
        'vid'  => $vid,
      ]);
      $term = $this->updateTermFields($term, $terme, $config['fields']);
      $term->enforceIsNew(TRUE)->setPublished(TRUE)->save();
      $new++;
    }

    $this->logger->get('taxonomy_dibasync')->info(new FormattableMarkup('Syncronizing term in vid: "@vid". Created: @created, deleted: @deleted, updated: @updated, unpublished: @unpublished.', [
      '@vid'         => $vid,
      '@created'     => $new,
      '@deleted'     => $delete,
      '@updated'     => $update,
      '@unpublished' => $unpublish,
    ]));

    return TRUE;
  }

  /**
   * Update term fields using origin setting.
   */
  private function updateTermFields($term, $data, $fields) {
    foreach ($fields as $field_key => $value) {
      if (isset($value['origin'])) {
        if (is_string($value['origin'])) {
          $data_field = $this->getDataFromOrigin($data, $value['origin']);
          if (!empty($data_field)) {
            $term->set($field_key, $data_field);
          }
        }
        elseif (is_array($value['origin'])) {
          foreach ($value['origin'] as $origin) {
            $data_field = $this->getDataFromOrigin($data, $origin);
            if (!empty($data_field)) {
              $term->set($field_key, $data_field);
            }
          }
        }
      }
    }

    return $term;
  }

  /**
   * Get data from origin value property.
   */
  private function getDataFromOrigin($data, $origin) {
    if (FALSE === strpos($origin, '.')) {
      return isset($data[$origin]) ? $data[$origin] : '';
    }
    else {
      $explode = explode('.', $origin);
      // String before the point.
      $left_origin = $explode[0];
      unset($explode[0]);
      // String after the point.
      $right_origin = implode('.', $explode);
      // Recursive call.
      return isset($data[$left_origin]) ? $this->getDataFromOrigin($data[$left_origin], $right_origin) : '';
    }
  }

}
