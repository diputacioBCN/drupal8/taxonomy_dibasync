<?php

namespace Drupal\taxonomy_dibasync\Services;

/**
 * Defines data collector interface.
 */
interface DataCollectorInterface {

  /**
   * Sync dataset data with drupal vocabulary.
   */
  public function syncData(string $type);

}
