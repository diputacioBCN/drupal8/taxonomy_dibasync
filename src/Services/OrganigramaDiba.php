<?php

namespace Drupal\taxonomy_dibasync\Services;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\taxonomy_dibasync\Services\DataCollectorInterface;
use Drupal\taxonomy_dibasync\Services\DataCollectorTrait;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Organigrama DiBa from GGRL schema.
 */
class OrganigramaDiba implements DataCollectorInterface, ContainerInjectionInterface {

  use DataCollectorTrait;
  use MessengerTrait;

  /**
   * The module settings.
   */
  protected $config;

  /**
   * The Entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Mapping dataset/vocabulary settings.
   */
  private $settings = [
    'fields' => [
      'name' => [
        'name'   => 'Nom',
        'origin' => 'org_nom',
      ],
      'field_org_codisap' => [
        'name'   => 'Codi SAP',
        'origin' => 'org_codisap',
      ],
      'field_org_pare' => [
        'name'   => 'Orgànic pare',
        'origin' => 'org_pare',
      ],
      'field_org_nivell' => [
        'name'   => 'Nivell',
        'origin' => 'org_nivell',
      ],
      'field_org_reduit' => [
        'name'   => 'Nom reduït',
        'origin' => 'org_reduit',
      ],
    ],
    'fields_map' => [
      'field_org_codisap' => 'org_codisap',
      'name'              => 'org_nom',
    ],
    'parent_field'  => 'field_org_pare',
    'primary_field' => 'field_org_codisap',
  ];

  /**
   * {@inheritdoc}
   */
  public function syncData(string $type) {
    $vid = $this->config->get('organigrama_target');

    // Pass settings trought massage to invoke alter hooks.
    $settings = $this->massageSettings($type, $this->settings);

    if (!empty($vid) && $this->buildVocabularyFields($vid, $settings['fields'])) {
      $organigrama = $this->getData();

      if (!empty($organigrama)) {
        $config = [
          'fields' => $settings['fields'],
          'fields_map' => $settings['fields_map'],
          'missing_action' => $this->config->get('organigrama_missing'),
          'duplicate_action' => $this->config->get('organigrama_duplicate'),
        ];
        $this->syncDataTerms($vid, $organigrama, $config);

        // Un cop creats/actualitzats/esborrats tots els termes establim les
        // dependencies. Recuperem tot de nou tot amb els canvis aplicats.
        $terms = $this->loadTerms($vid);
        if (!empty($terms)) {
          foreach ($terms as $term) {
            $tid_pare = '';
            if ($codi_pare = $term->get($settings['parent_field'])->getString()) {
              $tid_pare = $this->getTidFromCodi($settings['primary_field'], $codi_pare);
            }
            if (!empty($tid_pare)) {
              $term->set('parent', $tid_pare)->save();
            }
          }
        }
      }
    }
  }

  /**
   * Get collector data.
   */
  private function getData() {
    $dbh = oci_connect($this->config->get('ora_user'), $this->config->get('ora_pass'), $this->config->get('ora_host'), $this->config->get('ora_charset'));
    if (!$dbh) {
      $this->messenger()->addError("No es pot recuperar l'organigrama: Error de connexió al GGRL - " . serialize(oci_error()));

      return [];
    }

    $sth = oci_parse($dbh, 'SET ROLE ALL');
    oci_execute($sth);

    $sth = oci_parse($dbh, $this->getQuery());
    oci_execute($sth);

    $nrows = oci_fetch_all($sth, $result, 0, -1, OCI_FETCHSTATEMENT_BY_ROW + OCI_ASSOC);
    oci_free_statement($sth);

    if (0 === $nrows) {
      $this->messenger()->addError("No es pot recuperar l'organigrama: Dades no disponibles.");

      return [];
    }

    // Cal convertir tots els strings a UTF-8 i eliminar certs orgànics.
    $orgs = [];
    foreach ($result as $org) {
      $tipus = strtolower(substr($org['GGRL_ORG_REDUIT'], 0, 1));
      $exclosos = ['s', 'z'];
      $charset_in = $this->config->get('ora_iso_charset');
      $charset_out = $this->config->get('drupal_charset');
      if (!in_array($tipus, $exclosos)) {
        $orgs[] = [
          'org_nom'     => trim(iconv($charset_in, $charset_out, $org['GGRL_ORG_DESC_AMPL'])),
          'org_codisap' => iconv($charset_in, $charset_out, $org['GGRL_ORG_CODISAP']),
          'org_pare'    => iconv($charset_in,$charset_out, $org['GGRL_ORG_DEP_CODISAP']),
          'org_nivell'  => iconv($charset_in, $charset_out, $org['GGRL_ORG_NIVELL']),
          'org_reduit'  => trim(iconv($charset_in, $charset_out, $org['GGRL_ORG_REDUIT'])),
        ];
      }
    }

    return $orgs;
  }

  /**
   * Get Oracle native query.
   */
  private function getQuery() {
    return 'SELECT GGRL_ORG_CODISAP,
        GGRL_ORG_REDUIT,
        GGRL_ORG_NIVELL,
        GGRL_ORG_DEP_CODISAP,
        GGRL_ORG_DESC_AMPL
      FROM GGRL_SAP_V_ORGANICS
      WHERE GGRL_ORG_CODISAP <> 50088460 AND GGRL_ORG_NIVELL <= ' . $this->config->get('organigrama_level') . '
      ORDER BY GGRL_ORG_REDUIT';
  }

  /**
   * Torna el tid a partir del codi sap.
   */
  private function getTidFromCodi($primary_field, $codi) {
    $tid = $this->entityTypeManager
      ->getStorage('taxonomy_term')
      ->getQuery()
      ->condition('vid', $this->config->get('organigrama_target'))
      ->condition($primary_field, $codi)
      ->accessCheck(FALSE)
      ->execute();

    return !empty($tid) ? reset($tid) : '';
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityFieldManagerInterface $entity_field_manager, EntityTypeManagerInterface $entity_type_manager, $logger, ModuleHandlerInterface $module_handler) {
    $this->config = $config_factory->get('taxonomy_dibasync.settings');
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_field.manager'),
      $container->get('entity_type.manager'),
      $container->get('logger.factory'),
      $container->get('module_handler')
    );
  }

}
